<?php

namespace App\Controller;

use App\Entity\Currency;
use App\Repository\CurrencyRepository;
use App\Service\CurrencyService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CurrencyController extends AbstractController
{
    public function __construct(private HttpClientInterface $httpClient, private CurrencyService $currencyService, private CurrencyRepository $currencyRepository)
    {
        $this->httpClient = $httpClient;
        $this->currencyService = $currencyService;
        $this->currencyRepository = $this->currencyRepository;
    }

    #[Route('/process', name: 'currency_process')]
    public function process(): Response
    {
        $request = $this->httpClient->request('GET','https://api.nbp.pl/api/exchangerates/tables/a/');
        $response = $request->toArray();

        if(!sizeof($response)) {
            throw new \Exception('Pusta tablica');
        }
        if(!array_key_exists('rates',$response[0])) {
            throw new \Exception('Niepoprawna struktura tablicy');
        }

        $response = $response[0]['rates'];
        $collection = $this->currencyService->denormalize($response);
        $currentRecords = $this->currencyRepository->findAll();

        /*
         * PUT
         * $collection[32]->setMid(12.456);
         */


        /*
         * POST
         * $new = new Currency();
            $new
                ->setCurrency('test')
                ->setCode('TTT')
                ->setMid(99.999999999);
            array_push($collection,$new);
         */

        $this->currencyService->update($currentRecords,$collection);

        return $this->redirectToRoute('currency_list');
    }


    #[Route('/list', name: 'currency_list')]
    public function list(): Response
    {
        return $this->render('currency/index.html.twig', [
            'list'=>$this->currencyRepository->findAll()
        ]);
    }
}
