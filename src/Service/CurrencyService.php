<?php

namespace App\Service;

use App\Entity\Currency;
use App\Repository\CurrencyRepository;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\DBAL\Exception\DatabaseDoesNotExist;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CurrencyService
{
    public function __construct(private EntityManagerInterface $entityManager, private CurrencyRepository $currencyRepository)
    {
        $this->entityManager = $this->entityManager;
        $this->currencyRepository = $this->currencyRepository;
    }

    public function denormalize(array $array): array
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $normalizer = new ObjectNormalizer($classMetadataFactory);
        $serializer = new Serializer([$normalizer]);

        $collection = [];
        foreach ($array as $record) {
            $entity = $serializer->denormalize($record,Currency::class);
            array_push($collection,$entity);
        }

        return $collection;
    }

    public function update(array $currentRecords, array $collection): void
    {
        if(!sizeof($currentRecords)) {
            foreach ($collection as $entity) {
                $this->entityManager->persist($entity);
                $this->entityManager->flush();
            }
        }
        if(!sizeof($collection)) {
            throw new \Exception('Brak danych do aktualizacji!');
        }
        else {
            foreach ($collection as $entity)
            {
                $mid = null;
                $flag = true;
                $current = null;

                foreach ($currentRecords as $record)
                {
                    if($entity->getCode() === $record->getCode()) {
                        $current = $record;
                        $mid = $entity->getMId();
                        $flag = false;
                    }
                }
                if($flag) {
                    //POST
                    var_dump('POST');
                    $this->entityManager->persist($entity);
                    $this->entityManager->flush();
                }
                else {
                    //PUT
                    if((float)$current->getMid() !== (float)$mid) {
                        echo 'aktualizacja';
                        $this->currencyRepository->updateRate($current,$mid);
                    }
//                    else {
//                        echo "rozne<br>";
//                    }

                }
            }
        }
    }

}